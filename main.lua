require("vector") 
require("obstacle") 
require("ray")
require("radiant")
require("radiantUnique")
require("radiantSector")
require("boid")

function love.load() 
    width = love.graphics.getWidth() 
    height = love.graphics.getHeight() 
    
 
    obstacles = {} 
 
    local points = { { 0, 0 }, { width, 0 }, { width, height }, { 0, height }, { 0, 0 } } 
    obstacles[1] = Obstacle:create(points) 
 
    points = { { 100, 100 }, { 100, 200 }, { 200, 200 }, { 200, 100 }, { 100, 100 } } 
    obstacles[2] = Obstacle:create(points) 
 
    points = { { 500, 100 }, { 650, 100 }, { 650, 300 }, { 500, 100 } } 
    obstacles[3] = Obstacle:create(points) 
 
    points = { { 450, 400 }, { 650, 500 }, { 480, 600 }, { 380, 420 }, { 450, 400 } } 
    obstacles[4] = Obstacle:create(points) 
 
    points = { { 80, 300 }, { 140, 300 }, { 140, 470 }, { 120, 470 }, { 80, 300 } } 
    obstacles[5] = Obstacle:create(points) 

    radiant = RadiantSector:create(10)
    segments = {}
    for i = 1, #obstacles do
        points = obstacles[i].points
        
        for j=2, #points do
            table.insert(segments, {points[j-1], points[j]})
        end

    end
    print(segments[1])
    for i=1, #segments do
        segments[i].show = false
    end

    boid = Boid:create(200, 200, radiant)


end 
 
function love.update(dt) 
    x, y = love.mouse.getPosition()
    mouse = Vector:create(x, y)
    
    boid:seek(mouse)
    boid:update()
    boid:update_radiant(segments)

    --radiant:update(segments)

end 
 
function love.draw() 
    --for i = 1, #obstacles do 
    --    obstacles[i]:draw() 
    --end 

    r, g, b, a = love.graphics.getColor()
    love.graphics.setColor(1, 1, 1)

    for i=1, #segments do
        if (segments[i].show) then
            local p1 = segments[i][1]
            local p2 = segments[i][2]
            love.graphics.line(p1[1], p1[2], p2[1], p2[2])
        end
    end
    love.graphics.setColor(r, g, b, a)
    --radiant:draw()
    boid:draw()
end