Boid = {} -- "bird-oid object"
Boid.__index = Boid

function Boid:create(x, y, radiantSector)
    local boid = {}
    setmetatable(boid, Boid)
    boid.acceleration = Vector:create(0, 0)
    boid.velocity = Vector:create(math.random(-10, 10) / 10, math.random(-10, 10) / 10)
    boid.location = Vector:create(x, y)
    boid.r = 5
    boid.vertices = { 0, -boid.r * 2, -boid.r, boid.r * 2, boid.r, 2 * boid.r }
    boid.radiantSector = radiantSector
    boid.maxSpeed = 4
    boid.maxForce = 0.1
    boid.theta = 0
    boid.color = 255
    boid.cur_path = 1

    return boid
end

function Boid:seek(target)
    local desired = target - self.location
    local mag = desired:mag()
    desired:norm()

    if mag < 100 then
        local m = math.map(mag, 0, 100, 0, self.maxSpeed)
        desired:mul(m)
    else
        desired:mul(self.maxSpeed)
    end

    local steer = desired - self.velocity
    steer:limit(self.maxForce)
    self:applyForce(steer)
end


function Boid:boundaries()
    local desired = nil
    local d = 0
    if self.location.x < d then
        desired = Vector:create(self.maxSpeed, self.velocity.y)
    elseif self.location.x > width - d then
        desired = Vector:create(-self.maxSpeed, self.velocity.y)
    end

    if self.location.y < d then
        desired = Vector:create(self.velocity.x, self.maxSpeed)
    elseif self.location.y > height - d then
        desired = Vector:create(self.velocity.x, -self.maxSpeed)
    end

    if desired then
        desired:norm()
        desired:mul(self.maxSpeed)
        local steer = desired - self.velocity
        steer:limit(self.maxSpeed)
        self:applyForce(steer)
    end

end

function Boid:update()
    self.velocity:add(self.acceleration)
    self.velocity:limit(self.maxSpeed)
    self.location:add(self.velocity)
    self.acceleration:mul(0)
end

function Boid:update_radiant(segments)
    self.radiantSector:update(segments, self.location:copy(), self.theta)
end
function Boid:applyForce(force)
    self.acceleration:add(force)
end

function Boid:draw()
    r, g, b, a = love.graphics.getColor()    
    love.graphics.setColor(self.color/255, 0, self.color, 1)

    local theta = self.velocity:heading() + math.pi / 2
    self.theta = theta
    love.graphics.push()
    love.graphics.translate(self.location.x, self.location.y)
    love.graphics.rotate(theta)
    --love.graphics.polygon("fill", self.vertices)
    love.graphics.pop()
    love.graphics.setColor(r, g, b, a )
    
    self.radiantSector:draw()
end
