RadiantSector = {}
RadiantSector.__index = RadiantSector

function RadiantSector:create(angle)
    local radiant = {}
    setmetatable(radiant, RadiantSector)
    radiant.n = angle * 3
    radiant.position = { 400, 300 }
    radiant.rays = {}
    radiant.dxdy = {}
    radiant.theta = 0
    radiant.angle = angle

    for i=1, radiant.n do
        table.insert(radiant.rays, Ray:create(radiant.position, {0, 0}))
        local cur_angle = -angle + 2 * i * angle / radiant.n
        table.insert(radiant.dxdy, {math.cos(math.rad(cur_angle)), math.sin(math.rad(cur_angle))})
    end
    --table.insert(radiant.rays, Ray:create(radiant.position, {0, 0}))
    --table.insert(radiant.dxdy, {math.cos(math.rad(-angle)), math.sin(math.rad(-angle))})

    --table.insert(radiant.rays, Ray:create(radiant.position, {0, 0}))
    --table.insert(radiant.dxdy, {math.cos(math.rad(angle)), math.sin(math.rad(angle))})
    return radiant
end

function RadiantSector:update(segments, position, theta)
    self.theta = theta
    for i=1, radiant.n do
        local cur_angle = -self.angle + 2 * i * self.angle / self.n 
        self.dxdy[i] = {math.cos(math.rad(cur_angle) + self.theta - math.pi/2), math.sin(math.rad(cur_angle) + self.theta - math.pi/2)}
    end


    for i=1, #segments do 
        segments[i].show = false
    end
    --x, y = love.mouse.getPosition()
    x = position.x
    y = position.y
    self.position[1] = x
    self.position[2] = y
    for i=1, #self.rays do
        ray = self.rays[i]
        dxdy = self.dxdy[i]
        ray:lineTo({x + dxdy[1], y + dxdy[2]}, segments)
    end
end

function RadiantSector:draw()
    --print(self.dxdy[1][1])
    r, g, b, a = love.graphics.getColor()
    love.graphics.setColor(1, 0, 0)
    self.rays[1]:draw()
    self.rays[#self.rays]:draw()

    love.graphics.setColor(1, 1, 1)


    love.graphics.push()
    love.graphics.translate(self.position[1], self.position[2])
    love.graphics.rotate(self.theta - math.pi / 2)

    love.graphics.circle("line", 10, 0, 28)
    love.graphics.circle("fill", 20, 5, 4)
    love.graphics.circle("fill", 20, -5, 4)

    love.graphics.pop()


    --love.graphics.circle("line", 10, 0, 28)
    --love.graphics.circle("fill", 20, 5, 4)
    --love.graphics.circle("fill", 20, -5, 4)

    --love.graphics.pop()


    love.graphics.setColor(r, g, b, a)

end