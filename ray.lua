Ray = {}
Ray.__index = Ray

function Ray:create(origin, to)
    local ray = {}
    setmetatable(ray, Ray)
    ray.origin = origin
    ray.to = origin
    ray.intersections = {}
    ray.closest = nil
    return ray
end

function Ray:draw()
    r, g, b, a = love.graphics.getColor()
    love.graphics.setColor(1, 0.75, 0.75)
    local to = self.to
    if (self.closest ~= nil)  then 
        to = {self.closest.x, self.closest.y}
    end 
    love.graphics.line(self.origin[1], self.origin[2], to[1], to[2])

    for i=1, #self.intersections do
        local intersection = self.intersections[i]
        love.graphics.circle("fill", intersection.x, intersection.y, 3)
    end

    if (self.closest ~= nil) then
        love.graphics.setColor(1, 0, 0)
        love.graphics.circle("line", self.closest.x, self.closest.y, 5)
    end

    love.graphics.setColor(r, g, b, a)
end

function Ray:intersection(segment)
    local px = self.origin[1]
    local py = self.origin[2]
    local dx = self.to[1] - px
    local dy = self.to[2] - py

    local spx = segment[1][1]
    local spy = segment[1][2]
    local sdx = segment[2][1] - spx
    local sdy = segment[2][2] - spy

    local rmag = math.sqrt(dx * dx + dy * dy)
    local smag = math.sqrt(sdx * sdx + sdy * sdy)

    if (dx / rmag == sdx / smag) and (dy / rmag == sdy / smag) then
        return nil
    end

    local t2 = (dx * (spy - py) + dy * (px - spx)) / (sdx * dy - sdy * dx)
    local t1 = (spx + sdx * t2 - px) / dx

    if (t1 < 0) then
        return nil
    end

    if (t2 < 0 or t2 > 1) then 
        return nil
    end

    local x = px + dx * t1
    local y = py + dy * t1
    --print(x, y)
    return {x=x, y=y, t1=t1}

end


function Ray:lineTo(to, segments)
    self.to = to
    local intersections = {}
    self.closest = nil
    local closest_index = nil
    for i=1, #segments do
        local intersection = self:intersection(segments[i])
        
        if (intersection ~= nil) then
            --table.insert(intersections, intersection)
            if (self.closest == nil) then
                self.closest = intersection
                closest_index = i
            else
                if (intersection.t1 < self.closest.t1) then
                    closest_index = i
                    self.closest = intersection
                end
            end
        end 
    end
    if closest_index ~= nil then
        segments[closest_index].show = true
    end
    table.insert(intersections, self:intersection(segments[closest_index]))
    self.intersections = intersections
end